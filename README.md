# What's new in .NET 6 and C# 10 [![Open in Visual Studio Code](https://open.vscode.dev/badges/open-in-vscode.svg)](https://open.vscode.dev/NikiforovAll/whats-new-in-dotnet6)

All you need to do is to open repository in VSCode and run 'Remote-Containers: Open Folder in Container...'.

## Getting started

Please see related blog post for more information.
